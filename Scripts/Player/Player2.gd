extends KinematicBody2D

# Declare member variables here. Examples:
const UP = Vector2(0,-1)
var move = Vector2()
export var velocidade = 5
export var gravity = 300
export var jumpForce = 200
export var pulos = 1


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	move.y += gravity * delta
	if Input.is_action_pressed("ui_right"):
		move.x = velocidade
		$AnimatedSprite.animation = "walking"
		$AnimatedSprite.flip_h = true
	elif Input.is_action_pressed("ui_left"):
		move.x = -velocidade
		$AnimatedSprite.animation = "walking"
		$AnimatedSprite.flip_h = true	
	else:
		move.x = 0
		$AnimatedSprite.animation = "stand"
		
	if Input.is_action_pressed("ui_accept") :
		move.y = -jumpForce
		pulos-=1	
	#if is_on_floor():
		
			
	move_and_slide(move, UP)		
		
