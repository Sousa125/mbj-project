extends KinematicBody2D

# Declare member variables here. Examples:
const UP = Vector2(0,-1)
var move = Vector2()
export var velocidade = 250
export var gravity = 300
export var jumpForce = 200
export var pulos = 20
export var pontuacao = 0
onready var player_vars = get_node("/root/singleton")


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	move.y += gravity * delta
	if Input.is_action_pressed("ui_right"):
		move.x = velocidade
		$AnimatedSprite.animation = "walking"
		$AnimatedSprite.flip_h = false
	elif Input.is_action_pressed("ui_left"):
		move.x = -velocidade
		$AnimatedSprite.animation = "walking"
		$AnimatedSprite.flip_h = true	
	else:
		move.x = 0
		$AnimatedSprite.animation = "idle"
	if is_on_floor():
		move.y = 0
		pulos=2
	if Input.is_action_pressed("ui_accept") :
		#if pulos>0:
		$AnimatedSprite.animation = "jump"	
		move.y = -jumpForce
		pulos-=1	

		
			
	move_and_slide(move, UP)		
		



func _on_Moeda_coin_collected():
	player_vars.pontuacao += 1

func _on_Caixa_die():
	player_vars.vida -= 1
